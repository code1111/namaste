import {createStore, compose, applyMiddleware} from "redux";
import appReducer from '../reducers/appReducer';
import thunk from 'redux-thunk';

const stateData = {
    amount: null,
    dailyLimit:null,
    errorMessage: '',
    transactions: [],
    currentTime:null
};

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const saver = store => next => action => {
    let result = next(action);
    localStorage['redux-store'] = JSON.stringify(store.getState());
    return result
};
const storeFactory = (initialState=stateData) =>
    composeEnhancer(applyMiddleware(saver, thunk))(createStore)(
        appReducer,
        (localStorage['redux-store']) ?
            JSON.parse(localStorage['redux-store']) :
            stateData
    );

export default storeFactory;