import React from "react";
import Transaction from './Transaction';
import {API} from "../config";

const TransactionHistory = (props) => {
    const transactions = props.transactions;
    return (
        <div>
            {
                (typeof transactions == "undefined" || transactions.length === 0) ?
                    <p>No transactions.</p> :
                    transactions.map(elem =>
                        <Transaction
                            key={elem[1]}
                            amount={elem[0]}
                            operation={elem[2]}
                            date={elem[1]}
                            id={elem[3]}
                            api={API}
                            delete={props.delete}
                        />
                    )
            }
        </div>
    )
};


export default TransactionHistory;