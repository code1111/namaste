import React from "react";

const Transaction = (props) => {
    const MyDateString = ('0' + new Date(props.date).getDate()).slice(-2) + '/'
        + ('0' + (new Date(props.date).getMonth() + 1)).slice(-2) + '/'
        + new Date(props.date).getFullYear();
    return (
        <div className={'transaction'}>
            <div className={'transaction-block'}>
                {
                    (props.operation) ? <span className="glyphicon glyphicon-plus-sign plus"/> :
                        <span className="glyphicon glyphicon-minus-sign minus"/>
                }
                <div className={'transaction-info'}>
                    <div className={'transaction-amount'}>{props.amount}R</div>
                    <div className={'transaction-date'}>{MyDateString}</div>
                </div>
            </div>
            <div className={'transaction-delete'}><p id={props.id} onClick={props.delete}>X</p></div>
        </div>
    )
};

export default Transaction;