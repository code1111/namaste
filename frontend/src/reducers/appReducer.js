import C from '../constants';

const appReducer = (state = {}, action) => {
    switch (action.type) {
        case C.SET_MESSAGE:
            return {
                ...state,
                errorMessage: action.payload
            };
        case C.SET_AMOUNT:
            return {
                ...state,
                amount: action.payload
            };
        case C.SET_DAILY_LIMIT:{
            return {
                ...state,
                dailyLimit:action.payload
            }
        }
        case C.SET_CURRENT_TIME:{
            return {
                ...state,
                currentTime:action.payload
            }
        }
        case C.CLEAR_TRANSACTION:
            return {
                ...state,
                transactions:[]
            };
        case C.ACTION_TRANSACTION:
            return {
                ...state,
                transactions: [...state.transactions, action.payload]
            };
        case C.DELETE_TRANSACTION: {
            return {
                ...state,
                transactions: action.payload
            }
        }
        default:
            return state;
    }
};

export default appReducer;