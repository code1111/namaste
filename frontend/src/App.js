import React, {Component} from 'react';
import './App.scss';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import ConfigurePage from './pages/ConfigurePage';
import MainPage from './pages/MainPage';
import NotFound from './pages/NotFoundPage';

import MainMenu from './components/MainMenu';

class App extends Component {

    render() {
        return (
            <Router>
                <main className={'main'}>
                    <MainMenu/>
                    <Switch>
                        <Route exact path="/" component={ConfigurePage}/>
                        <Route path="/main" component={MainPage}/>
                        <Route component={NotFound}/>
                    </Switch>
                </main>
            </Router>
        );
    }
}

export default App;
