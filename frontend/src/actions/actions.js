import C from '../constants';
import {API} from '../config';

export const setValidationState = (errorMessage) => {
    return {
        type: C.SET_MESSAGE,
        payload: errorMessage
    }
};

export function showError(errorMessage) {
    return dispatch => {
        dispatch(setValidationState(errorMessage));
        setTimeout(() => {
            dispatch(setValidationState(""));
        }, 1000);
    }
}

export const clearTransactionHistory = () => {
    return {
        type: C.CLEAR_TRANSACTION
    }
};

export const setAmount = (amount) => {
    return {
        type: C.SET_AMOUNT,
        payload: amount
    }
};
export const setDayLimit = (dailyLimit) => {
    return {
        type: C.SET_DAILY_LIMIT,
        payload: dailyLimit
    }
};
export const setCurrentTime = (time) => {
    return {
        type: C.SET_CURRENT_TIME,
        payload: time
    }
};
export const doTransaction = (transaction) => {
    return dispatch => {
        fetch(`${API}/addTransaction`, {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(transaction)
        }).then(res => res.json())
            .then(data => {
                // console.log(data);
                dispatch(showError(data.errorMessage));
                transaction.push(data.id); //add to array ID from MongoDB
                // console.log(transaction);
                dispatch(ApplyTransaction(transaction))
            }).catch(error => {
            dispatch(showError(error));
        }).catch(error => {
            dispatch(showError(error));
        })
    }
};

export const deleteTransaction = (id, transaction, amount, dailyLimit) => {
    return dispatch => {
        //console.log(transaction);
        // console.log(id);
        console.log(amount);
        const dTransac = transaction.filter(function (el, index) {
            let searchValue = el[3];
            if (searchValue === id) {
                const delMoney = transaction[index][0];
                const operation = transaction[index][2];
                if (operation) {
                    const newAmount = parseFloat(amount) - parseFloat(delMoney);
                    const dayBalance = parseFloat(dailyLimit) - parseFloat(delMoney);
                    dispatch(setDayLimit(dayBalance));
                    dispatch(setAmount(newAmount));
                } else {
                    const newAmount = parseFloat(amount) + parseFloat(delMoney);
                    const dayBalance = parseFloat(dailyLimit) + parseFloat(delMoney);

                    dispatch(setAmount(newAmount));
                    dispatch(setDayLimit(dayBalance));
                }
            }
            return searchValue !== id
        });
        fetch(`${API}/delTransaction`, {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify({id: id})
        })
            .then(res => res.json())
            .then(data => {
                dispatch(showError(data.errorMessage));
                dispatch(deleteFromTransactions(dTransac));
            })
            .catch(error => {
                dispatch(showError(error));
            })
    }
};

function ApplyTransaction(transaction) {
    return {
        type: C.ACTION_TRANSACTION,
        payload: transaction
    }
}

function deleteFromTransactions(transactions) {
    return {
        type: C.DELETE_TRANSACTION,
        payload: transactions
    }
}
