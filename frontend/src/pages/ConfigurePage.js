import React, {Component} from 'react';
import {Button, InputGroup, InputGroupAddon, Col, Container, Jumbotron, Row} from "reactstrap";
import CurrencyInput from 'react-currency-input';

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {setAmount,setDayLimit,clearTransactionHistory, showError,setCurrentTime} from "../actions/actions";
import {getDaysInMonth} from '../helpers/lib';

class Configure extends Component {
    constructor(props) {
        super(props);
        this.handleSetAmount = this.handleSetAmount.bind(this);
    }

    handleSetAmount() {
        const amount = this.refs.currencyinput.getMaskedValue();
        if(amount ==='0.00' || typeof amount == "undefined") {
            this.props.showError('Please enter a valid value!');
        }else {
            const days = getDaysInMonth(new Date());
            const timeShift = days - (new Date().getDate());
            const dailyLimit = parseFloat(amount/timeShift).toFixed(2);
            this.props.setAmount(amount);
            this.props.setDayLimit(dailyLimit);
            this.props.setCurrentTime(new Date().toDateString());

            this.props.clearTransactionHistory();
            const {from} = {from: {pathname: "/main"}};
            this.props.history.push(from);
        }
    }

    render() {
        const {errorMessage} = this.props;
        return (
            <div>
                <Jumbotron>
                    <Container>
                        <Row>
                            <Col sm="12" md={{size: 6, offset: 3}}>
                                <div className={'monthly-amount'}>
                                    <h1>Monthly amount</h1>
                                    <InputGroup size="sm">
                                        <CurrencyInput
                                            id={'amount'}
                                            className={`form-control`}
                                            thousandSeparator={''}
                                            value={0.00}
                                            autoFocus={true}
                                            ref='currencyinput'
                                        />
                                        <InputGroupAddon addonType="prepend">
                                            <Button
                                                onClick={this.handleSetAmount}
                                                id={"amount-btn"}
                                                >Set</Button>
                                        </InputGroupAddon>
                                    </InputGroup>
                                    <div className="alert-info">{errorMessage}</div>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </Jumbotron>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        amount: state.amount,
        errorMessage: state.errorMessage,
        currentTime:state.currentTime
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        showError: bindActionCreators(showError, dispatch),
        setAmount: bindActionCreators(setAmount, dispatch),
        setDayLimit: bindActionCreators(setDayLimit, dispatch),
        clearTransactionHistory: bindActionCreators(clearTransactionHistory,dispatch),
        setCurrentTime:bindActionCreators(setCurrentTime, dispatch)
    }
};

const ConfigurePage = connect(mapStateToProps, mapDispatchToProps)(Configure);

export default ConfigurePage;