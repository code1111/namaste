import React, {Component} from 'react';
import Jumbotron from "reactstrap/es/Jumbotron";
import {Button, Col, Container, Row} from "reactstrap";
import { Redirect } from "react-router";
import CurrencyInput from 'react-currency-input';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import TransactionHistory from '../components/TransactionHistory';
import {showError,doTransaction,setAmount, deleteTransaction,setDayLimit, setCurrentTime} from "../actions/actions";
import {getDaysInMonth} from '../helpers/lib';


class MainPageComponent extends Component {
    constructor(props){
        super(props);
        this.clickHandle = this.clickHandle.bind(this);
    }

    clickHandle(e){
        const money = this.refs.transactionInput.getMaskedValue();
        const {amount, dailyLimit,currentTime} = this.props;
        if(currentTime !== (new Date().toDateString())){
            const days = getDaysInMonth(new Date());
            const timeShift = days - (new Date().getDate());
            const dailyLimit = parseFloat(amount/timeShift).toFixed(2);
            this.props.setCurrentTime(new Date().toDateString());
            this.props.setDayLimit(dailyLimit);
        }

        if(e.target.id === 'income'){
            if(money ==='0.00' || typeof money == "undefined") {
                this.props.showError('Please enter a valid value!');
                return false
            }

            const transaction_date = new Date().getTime();
            const operation = true;
            let newTransac = [money, transaction_date, operation];
            let newTotal =  parseFloat(amount) + parseFloat(money);

            const dayBalance = parseFloat(dailyLimit) + parseFloat(money);
            this.props.setDayLimit(dayBalance);

            this.props.doTransaction(newTransac);
            this.props.setAmount(newTotal);
        }
        if(e.target.id === 'expense'){
            if(money ==='0.00' || typeof money == "undefined") {
                this.props.showError('Please enter a valid value!');
                return false
            }

            const transaction_date = new Date().getTime();
            const operation = false;
            let newTransac = [money, transaction_date, operation];
            let newTotal = parseFloat(amount)- parseFloat(money);

            const dayBalance = parseFloat(dailyLimit) - parseFloat(money);
            this.props.setDayLimit(dayBalance);

            this.props.doTransaction(newTransac);
            this.props.setAmount(newTotal);
        }

    }

    render() {
        const {transactions,amount,dailyLimit,errorMessage} = this.props;
        if (amount === null || typeof amount == "undefined") {
            return <Redirect to={'/'}/>
        }
        const onDelete = (e) => {
            this.props.deleteTransaction(e.target.id, transactions,amount, dailyLimit);
        };
        return (
            <div>
                <Jumbotron>
                    <Container>
                        <Row>
                            <div className={'status-panel-block'} onClick={this.clickHandle}>
                                <div className={'status-panel'}>
                                    <h2>You have {parseFloat(amount).toFixed(2)}R left for the month</h2>
                                    <h2>{parseFloat(dailyLimit).toFixed(2)}R left for the day</h2>
                                </div>
                                <Row className={'control'}>
                                    <div className={'control-btn'}>
                                        <CurrencyInput
                                            id={'transaction-amount'}
                                            className={`form-control`}
                                            thousandSeparator={''}
                                            value={0.00}
                                            autoFocus={true}
                                            ref='transactionInput'
                                        />
                                    </div>
                                    <div className={'control-btn'}>
                                        <Button id={'expense'} color="danger">Expense</Button>
                                    </div>
                                    <div className={'control-btn'}>
                                        <Button id={'income'} color="primary">Income</Button>
                                    </div>
                                </Row>
                                <div className="alert alert-info err">{errorMessage}</div>
                            </div>

                        </Row>
                    </Container>
                </Jumbotron>
                <Jumbotron className={'history-block'}>
                    <Row>
                        <Col sm="12" md={{size: 6, offset: 3}}>
                            <div className={'status-panel'}>
                                <h2>History</h2>
                            </div>
                            <div className={'transaction-history'}>
                                <TransactionHistory transactions={transactions} delete={onDelete}/>
                            </div>
                        </Col>
                    </Row>
                </Jumbotron>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        amount: state.amount,
        errorMessage: state.errorMessage,
        transactions: state.transactions,
        dailyLimit: state.dailyLimit,
        currentTime: state.currentTime
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        showError: bindActionCreators(showError, dispatch),
        doTransaction: bindActionCreators(doTransaction, dispatch),
        setAmount: bindActionCreators(setAmount,dispatch),
        setDayLimit:bindActionCreators(setDayLimit,dispatch),
        deleteTransaction: bindActionCreators(deleteTransaction,dispatch),
        setCurrentTime: bindActionCreators(setCurrentTime, dispatch)
    }
};

const MainPage = connect(mapStateToProps, mapDispatchToProps)(MainPageComponent);

export default MainPage;