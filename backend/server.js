const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require('./config');
const errorHandler = require ('./helpers/error-handler');
const database = require('./database');
const addTransaction = require('./routes/addTransaction');
const delTransaction = require('./routes/delTransaction');

// express
const app = express();

// sets and uses
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// routes
app.use('/api', addTransaction);
app.use('/api', delTransaction);

// global error handler
app.use(errorHandler);

database().then(info => {
    console.log(`Connected to ${info.host}:${info.port}/${info.name}`);
    app.listen(config.PORT, function () {
        console.log('Server listening on port ' + config.PORT);
    });
}).catch(()=>{
    console.error('Unable to connect to database');
    process.exit(1);
});
