const express = require('express');
const router = express.Router();
const Transaction = require('../models/transaction');

// routes
router.post('/addTransaction',addTransaction);

function addTransaction(reg,res) {
    const transact = reg.body;
    const newTransact = new Transaction({transactions: transact});
    if(newTransact){
        newTransact.save(function (err, result) {
            if (err) return console.log(err);

            //console.log(result.id);
            return res.json({
                id: result.id,
                errorMessage: "Transactions added!"
            });
        });
    }
    else {
        return res.json({errorMessage: "There are no transactions to add!"});
    }
}

module.exports = router;