const express = require('express');
const router = express.Router();
const Transaction = require('../models/transaction');

// routes
router.post('/delTransaction', delTransaction);

function delTransaction(reg, res) {
    console.log(reg.body.id);
    const idTransaction = reg.body.id;

    if (idTransaction) {
        Transaction.deleteOne({_id: idTransaction}, function (err) {
            if (!err) {
                return res.json({
                    errorMessage: "Transaction removed!"
                });
            } else {
                return res.json({
                    errorMessage: 'Error! Transaction not removed!'
                })
            }
        });
    } else {
        return res.json({errorMessage: "There are no transactions to delete!"});
    }
}

module.exports = router;