const conf = require('./config');
const mongoose = require('mongoose');

module.exports = () => {
    return new Promise((resolve, reject) =>{
        mongoose.Promise = global.Promise;
        mongoose.set('debug', true);
        mongoose.set('useCreateIndex', true);

        mongoose.connection
            .on('error', error => reject(error))
            .on('close', () => console.log('Database connection closed.'))
            .once('open', () => resolve(mongoose.connections[0]));

        mongoose.connect(conf.MONGO_URL,{ useNewUrlParser: true });
    })
};