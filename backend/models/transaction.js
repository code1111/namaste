const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema(
    {
        transactions: {
            type: [],
        },
    },
    {versionKey: false},
    {timestamps: true}
);

schema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Transaction', schema);